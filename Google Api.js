      // Initialize and add the map
      function initMap() {
        // The location of Bakery/Cake Shop
        const Bakery = { lat: 20.593, lng: 78.962 };
        const map = new google.maps.Map(document.getElementById("map"), {
          zoom: 4,
          center: Bakery,
        });
        // The marker, positioned at Bakery
        const marker = new google.maps.Marker({
          position: Bakery,
          map: map,
        });
      }